# The new src setup

The new setup you can use instead of the original one in the Occhio Theme.

This repository contains no libraries because one of the tagets is to keep the files as small as possible.
You can add a library like you're used to!

## How to use
This setup is as plain as possible, so you will need to write your own basics. Here I will explain how to do that.

### JavaScript
JavaScript is now modular, so you will need to write a module for every piece of script.
In the ```script.js```, there is an example to require a new module.

### Sass
#### Mixins
This setup works with a lot of mixins. There are mixins to make a container, a grid etc. I advice you to take a look inside it before you start your project.

If you have a good new mixin, please make a Pull Request to add it to this repository.

All the basics are included in the ```main.scss```.

#### Fonts
A way to include a font is explained in the file *sass/base/font.scss*.

There is also a Mixin to set a dynamic font size.
```
//  Cross-resolution style setting
@mixin adjust($map, $property: "font-size") {
  @each $item, $i in $map {
    & {
      @if( $item == "xs") {
        #{$property}: $i;
      }
      @if( $item == "sm") {
        @include breakpoint($adjust-sm) {
          #{$property}: $i;
        }
      }
      @if( $item == "md") {
        @include breakpoint($adjust-md) {
          #{$property}: $i;
        }
      }
      @if( $item == "lg") {
        @include breakpoint($adjust-lg) {
          #{$property}: $i;
        }
      }
    }
  }
}
```

To use it, set a map in the variables.scss like this:
```
$base-font-size: (
  xs: 1rem,
  sm: 1.1rem,
  md: 1.125rem,
  lg: 1.125rem   // 18px
);
```

Include the mixin with the map as parameter in the element you want like this:
```
.element {
  @include adjust($base-font-size);
}
```


Good luck!
For questions you can contact Martijn. For additions you can make a Pull Request!
